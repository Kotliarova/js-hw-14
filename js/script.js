
const btnChengeSubject = document.querySelector('.change-subject');
const styleCss = document.querySelector('.style-css');
const linkCss = localStorage.getItem('linkCss');
styleCss.href = `${linkCss}`;

if(!localStorage.getItem('subject') || !localStorage.getItem('linkCss')) {
    setSubject('sun');
    styleCss.href = 'css/style.css';
}

function setSubject(subject) {
    localStorage.setItem('subject', subject);
    if(subject === 'sun') {
        localStorage.setItem('linkCss', 'css/style.css');
    } else {
        localStorage.setItem('linkCss', 'css/moon-style.css');
    }
}

btnChengeSubject.addEventListener('click', evnt => {
    if(localStorage.getItem('subject', 'sun') === 'sun'){
        setSubject('moon');
    } else {
        setSubject('sun');
    }
    let subject = localStorage.getItem('subject');
    if(subject === 'moon') {
        styleCss.href = 'css/moon-style.css';
    } else {
        styleCss.href = 'css/style.css';
    }
})
